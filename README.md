This app is integrated to HSL APIs and is used to track certain vehicles, e.g., busses, city bikes. 




React is used to develop the frontend and backend will be done with Node. GitLab is used in version control.

GitLabs' issue tracking will be used as a backlog for features so devs can communicate work more easily


### **Running frontend only**

Comment out the script in index.html if you want to use React Developer Tools

Install the dependencies by running: **npm install**

check coding style: **npm run lint**


start the app by running: **npm run dev**


### **Running the whole app**

Install the dependencies by running: **npm install**

Use Webpack to bundle the app: **webpack -p**

Run the server: **npm start** or **npm run server**

Finally navigate to localhost:8080

### **Deploying to heroku**

add the files to version control: **git add --all**

write a commit message e.g.: **git commit -m "Heroku deployment"**

push the changes to Heroku: **git push heroku master**

The app can be seen in

#### https://hslapp.herokuapp.com/


to make the linter run before a change is committed, see https://medium.com/the-node-js-collection/why-and-how-to-use-eslint-in-your-project-742d0bc61ed7



#### Links:


https://www.hsl.fi/avoindata

https://digitransit.fi/en/developers/apis/4-realtime-api/vehicle-positions-2/

https://digitransit.fi/en/developers/apis/1-routing-api/1-graphiql/

