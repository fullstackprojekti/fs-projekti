let express = require("express")
let app = express()
let mqtt = require("mqtt")

let url = "mqtts://mqtt.hsl.fi/"
let options = {
	clientId: "myCustomeClient",
	port: 8883,
	keepalive:60
}
let client = mqtt.connect(url, options)

// TODO: Refactor so there's no global data structure / variable
let data = []

const onMessage = (topic,message) => {
	let mes = JSON.parse(message)

	data.unshift({
		vehicleNumber: mes.VP.veh,
		lat: mes.VP.lat,
		long: mes.VP.long,
		spd: Math.round(mes.VP.spd * 3.6),
		route: mes.VP.route,
		desi: mes.VP.desi,
		dl: mes.VP.dl
	})
	data.length >= 100 ? data=data.slice(0,90) : null // So the server doesnt overflow with data and searching is reasonable

}

/**
 * @returns e.g.{ '835':
   [ { vehicleNumber: 835, lat: 60.19279, long: 24.908331 },
     { vehicleNumber: 835, lat: 60.192837, long: 24.908165 },
     { vehicleNumber: 835, lat: 60.192908, long: 24.90801 } ],
  '852':
   [ { vehicleNumber: 852, lat: 60.240701, long: 24.741877 },
     { vehicleNumber: 852, lat: 60.240681, long: 24.741841 },
     { vehicleNumber: 852, lat: 60.24066, long: 24.741805 } ],
  '870':
   [ { vehicleNumber: 870, lat: 60.236156, long: 24.728102 },
     { vehicleNumber: 870, lat: 60.236222, long: 24.728356 } ],
  '1071':
   [ { vehicleNumber: 1071, lat: 60.192795, long: 24.908887 },
     { vehicleNumber: 1071, lat: 60.192751, long: 24.909009 } ] }
 */
const groupData = () => {
	let groupedByVehicleNumber = data.reduce((values, element) => {
		values[element.vehicleNumber] = [...values[element.vehicleNumber]||[],element]
		return values
	},{})
	return groupedByVehicleNumber
}

const mqttSubScribe = () =>  {
}

client.on("message", onMessage)

// Couldn't get mqtt to work on browser not even over websockets, so we just send the data over http
app.use("/mqtt/:busId",(req,res) => {

	let routeNumber = req.params.busId
	client.subscribe(`/hfp/v2/journey/+/vp/+/+/+/${routeNumber}/#`,mqttSubScribe)

	let groupedArray = Object.values(groupData())
	let mostRecentBusses = []
	groupedArray.forEach(x => mostRecentBusses.push(x[0])) // the first one is the most recent one
	res.send(mostRecentBusses)

})

module.exports = app