/* eslint-disable no-undef */
const express = require("express")
const path = require("path")
const mongo = require("./backend/mongo")
const stationAPI = require("./backend/getData")
const port = process.env.PORT || 8080;
const app = express();

//process.env.TZ ="Europe/Helsinki"

const saveToDb = async (date) => {
	const day = date.getDay()
	const hour = date.getHours()==0 ? "00" : date.getHours().toString() // to pad the time with zeroes
	const minute = date.getMinutes()==0 ? "00": date.getMinutes().toString()

	let stationsPromise = stationAPI.getAll().then(response => response.data.bikeRentalStations)

	stationsPromise.then(stations => stations.map(station => {
		let stationEntry = new mongo.Station({...station, day, time: hour+minute})
		stationEntry.save()
	}))
}

mongo.connectDB().then(async () => {
	/*let rule = new schedule.RecurrenceRule()
	rule.minute = [0,15,30,45]

	schedule.scheduleJob(rule,(fireDate) => { // save the stations every 15 minutes

		saveToDb(fireDate)
		console.log("Saved an entry")
	})*/

}).catch(error => {
	console.log("Couldn't connect to the DB "+error)
})

app.use(express.static(__dirname));

app.get("/", (req, res) => {
	res.sendFile(path.resolve(__dirname+ "/dist", "index.html"));
});

app.get("/api/:stationId",(req,res) => {
	const id = req.params.stationId
	const day = req.query.day
	mongo.Station.getStation(id, day).then(station => {
		let groupedByTime = station.reduce((values,dbEle) => {
			values[dbEle.time] =[...values[dbEle.time] || [], dbEle]
			return values
		}, {})
		let ret = calculateAverageForTimes(groupedByTime)
		res.send(ret)
	})
}
)

app.get("/mqtt/:busId",require("./mqtt"))
/** Takes an object that is grouped by times as an argument
 * Outputs an object literal with the time as a key and average as a value
 * e.g. "1030": 2
*/
const calculateAverageForTimes = (groupedByTime) => {
	const groupedArray = Object.values(groupedByTime)
	const timesAndAverages = {}
	groupedArray.forEach(timeSlot => {
		let sum = 0
		timeSlot.forEach( entry => {
			sum += entry.bikesAvailable
		})
		let count = Object.keys(timeSlot).length
		let average = Math.round(sum / count)
		// TODO: Works only if there's at least 1 entry
		timesAndAverages[timeSlot[0].time] = average

	})
	return timesAndAverages
}

/*setInterval(() => {
	console.log("Wake up dummy")
	http.get("http://hslapp.herokuapp.com/")
}, 600000 ); //10 minutes
*/
app.listen(port);