/* eslint-disable no-console */
/* eslint-disable no-undef */
const fs = require("fs")
const mongoose = require("mongoose")

let secret
if(!process.env.MONGO_URI) {
	try {
		secret = fs.readFileSync("./secret.txt","utf8").trim()
		console.log("SECRET read")
	} catch(e){
		console.log("No secret found")
	}
}

const url = process.env.MONGO_URI || secret
const connectDB = () => {
	return mongoose.connect(url)
}

const stationSchema = new mongoose.Schema({
	name: String,
	bikesAvailable: Number,
	stationId: String,
	day: Number,
	time: String, // We don't want to lose the leading zeroes
})

stationSchema.statics.getStation = function (stationId, day) {
	return new Promise((resolve,reject) => { // TODO: Drop name and id too?
		this.find({stationId,day},{_id:0, __v:0}, (err,docs) => {
			if(err){
				console.log(err)
				return reject(err)
			}
			resolve(docs)
		})
	})
}
const Station = mongoose.model("Station",stationSchema)

module.exports = {connectDB, Station}