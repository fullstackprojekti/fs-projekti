

## SETUP

1. Go to:

https://gitlab.com/fullstackprojekti/fs-projekti

and click FORK. In your own fork, e.g., 
https://gitlab.com/akort/fs-projekti/project_members invite the group (fullstackProjekti) to share your project


2. In the fork, copy the link in the SSH box

(git@gitlab.com:akort/fs-projekti.git)

and clone the repo e.g.,
**git clone git@gitlab.com:akort/fs-projekti.git** .

3. Add an upstream remote to the project repo (cd into the new folder first).
**git remote add upstream git@gitlab.com:fullstackprojekti/fs-projekti.git**

4. verify that the output by running 

**git remote -v**

Output should be something like:

origin	git@gitlab.com:akort/fs-projekti.git (fetch)

origin	git@gitlab.com:akort/fs-projekti.git (push)

upstream	git@gitlab.com:fullstackprojekti/fs-projekti.git (fetch)

upstream	git@gitlab.com:fullstackprojekti/fs-projekti.git (push)



## Working with GIT

0. Before doing any work, make sure your master is up to date

**git checkout master**

**git pull upstream master**

1. First create a branch ,e.g.,
**git checkout -b testing**

2. write some code, then add the files you created to the commit
**git add --all**

3. commit the changes and write a commit message:
**git commit -m " Added a readme file"**

4. push the new branch to the personal repo
**git push --set-upstream origin testing**

5. copy the link from the terminal into the browser and create a merge request make sure the target is the project repo (not your personal repo). Have someone merge your code :))
