import React from "react"
import ReactDOMServer from "react-dom/server"
import { Map as LeafletMap , Marker, Popup, TileLayer, Polyline} from "react-leaflet"
import bikes from "../bikes"
import {DivIcon} from "leaflet"
import PMarker from "./PercentMarker"
import Chart from "./Chart"
import Message from "./Message"
import polyline from "polyline"
class Map extends React.Component {

	constructor() {
		super()
		this.state = {
			stations: [],
			nearestStations:[],
			userLocation:[0.0,0.0],
			zoom: 13,
			showLocation: false,
			center:[60.17,24.94],
			stationData:{},
			chartStationId:-609,
			showChart: false,
			error: {
				type: 0, // 0 for message, 1 for warning, -1 for error
				message: "Click a station to refresh"
			},
			route:[]
		}
	}

	componentDidMount() {
		// Get the user's location
		navigator.geolocation.getCurrentPosition(
			(position) => {
				this.setState({userLocation: [position.coords.latitude,position.coords.longitude], zoom:15, showLocation: true})
			},() => {
				this.setState({error: {message: "Turn on the location services", type: 1}})
			})
		// Get all bikestations and loop all the stations and push each in the array
		// and finally set the array as the state
		bikes.getAll().then( response => {
			var stations = [] // TODO: Use map and reduce
			response.data.bikeRentalStations.forEach(station => {
				stations.push(station)
			});
			this.updateNearest(stations) // To get the nearest stations
		}).catch(() => {
			this.setState({error: {message: "Error getting the stations from the HSL API", type: -1}})
		})
	}

	updateNearest = (stations) => {
		// Find the nearest stations
		bikes.getNearestStation(this.state.userLocation[0], this.state.userLocation[1]).then( edges => {
			let nearestId = edges.map(edge => edge.node.place.id)

			let stationProms = stations.map(
				station => { return(
					nearestId.includes(station.id) ? // if the station is nearest add a flag and a distance field to it
						bikes.getDistance([this.state.userLocation[0], this.state.userLocation[1]], [station.lat,station.lon])
							.then(distance => ({ ...station, distance, nearest: true })) : station) // else return the station
				}
			)
			Promise.all(stationProms).then(stations => this.setState({stations: stations})) // Wait for all the promises to resolve and set the state accordingly
		})
	}

	isMobileDevice = () => {
		if( navigator.userAgent.match(/Android/i)
			|| navigator.userAgent.match(/webOS/i)
			|| navigator.userAgent.match(/iPhone/i)
			|| navigator.userAgent.match(/iPad/i)
			|| navigator.userAgent.match(/iPod/i)
			|| navigator.userAgent.match(/BlackBerry/i)
			|| navigator.userAgent.match(/Windows Phone/i)
		){
			return true
		}
		else {
			return false
		}}

	handleClick = (event) => {
		let id = event.target.options.children.key
		if(id==609609){
			navigator.geolocation.getCurrentPosition(
				(position) => {
					this.setState({userLocation: [position.coords.latitude,position.coords.longitude], zoom:15, showLocation: true})
					this.updateNearest(this.state.stations)
				},() => {
					this.setState({error: {message: "Turn on the location services", type: 1}})
				})
			return // We don't want to use the user marker to get the stations
		}
		// Refresh all the stations when a marker is clicked
		bikes.getAll().then( response => {
			let stations = [] // TODO: Use map and reduce
			response.data.bikeRentalStations.forEach(station => {
				stations.push(station)
			});
			this.updateNearest(stations)
		}).catch(() => {
			this.setState({error: {message: "Error getting the stations from the HSL API", type: -1}})
		})

		const day = new Date().getDay()

		if(!this.isMobileDevice()){ // No need to get the data if the user is in mobile
			this.setState({error: {message: "Fetching data", type: 0}}) // Display fetching data to the user
			bikes.getStationDB(id,day).then(stationData => {
				this.setState({stationData})
				this.setState({chartStationId: id, showChart: true})
				this.setState({error: {message: "Data fetched",type: 0}})

			}).catch(() => {
				this.setState({error: {message: "Couldn't get the station data from the internal database", type: -1}})
			})
		}
	}

	updateChart = (e,day) => {
		e.preventDefault()
		this.setState({error: {message: "Fetching data", type: 0}})
		bikes.getStationDB(this.state.chartStationId,day).then(stationData => {
			this.setState({stationData})
			this.setState({error: {message: "Data fetched",type: 0}})

		}).catch(() => {
			this.setState({error: {message: "Couldn't refresh the data from the database", type: -1}})
		})
	}

	toggleDiv = (e) => {
		e.preventDefault()
		if(e.target.className) { // button doesnt have a className so clicking it doesnt hide the div
			this.setState(previous => ({
				showChart: !previous.showChart}))
		}
	}

	locateUser =(e) => {
		e.preventDefault()
		navigator.geolocation.getCurrentPosition(
			(position) => {
				this.setState({userLocation: [position.coords.latitude,position.coords.longitude], zoom:15, showLocation: true})
			},() => {
				this.setState({error: {message: "Turn on the location services", type: 1}})
			})
	}

	navigate =(e, lat, lon) => {
		e.preventDefault()
		navigator.geolocation.getCurrentPosition(
			(position) => {
				this.setState({userLocation: [position.coords.latitude,position.coords.longitude], zoom:15, showLocation: true})
				this.setState({error: {message: "Navigating", type: 0}})
				bikes.getRoute([position.coords.latitude,position.coords.longitude],[lat,lon]).then(x => {
					let distance = 0
					let route = x.legs.map( x => {
						distance += x.distance
						return polyline.decode(x.legGeometry.points)
					})
						.reduce((acc,val) => acc.concat(val)) // flatten the arrays
					this.setState({error: {message: "Distance to stop " + Math.floor(distance) + " meters" , type: 0}})
					this.setState({route})
				})
			},() => { // If the user clicked "no" or failed otherwise
				this.setState({error: {message: "Turn on the location services", type: 1}})
			})
	}
	render() {

		const user = this.state.showLocation ? <Marker onClick={this.handleClick} key = {609609609} position={[this.state.userLocation[0], this.state.userLocation[1]]}>
			<Popup key = {609609} >You</Popup>
		</Marker> : null


		// TODO: if there are more bikes in the station than the capacity, the max capacity increases on the map
		const stationMarkers = this.state.stations.map((station,i) => {
			let icon = new DivIcon({
				className: "custom-icon",
				html: ReactDOMServer.renderToString(<PMarker  bikesAvailable={station.bikesAvailable} spacesAvailable={station.spacesAvailable} nearest={station.nearest}/>)
			}) // TODO: CLEANUP
			return (
				<Marker onClick = {this.handleClick} key ={i} position={[station.lat,station.lon]} icon = {icon}>
					{station.nearest ? // if the station has a nearest flag we can display distance data
						<Popup key = {station.stationId}>
							{station.name} {station.bikesAvailable} / {station.bikesAvailable+station.spacesAvailable}<br></br>
						Distance: {Math.floor(station.distance.walkDistance)}m Time: {Math.ceil(station.distance.walkTime/60)}min
							<button onClick={e => this.navigate(e, station.lat, station.lon)}> Navigate </button>
						</Popup>
						: // otherwise return the normal data
						<Popup key = {station.stationId}>
							{station.name} {station.bikesAvailable} / {station.bikesAvailable+station.spacesAvailable}
							<button onClick={e => this.navigate(e, station.lat, station.lon)}> Navigate </button>
						</Popup>
					}
				</Marker>
			)
		})

		const buttonStyle = {
			width: "14%"
		}

		const chartAreaStyle = {
			position:"absolute",
			top: 29, right:0, border:0,
			zIndex:1000, opacity: 0.85, width: "30%",backgroundColor: "#c8e2ab"
		}
		const locationButtonStyle = {
			position:"absolute",
			backgroundColor: "#b3b3ff",
			top: "15%", left:0, border:"2px solid black",
			borderRadius: "50%",
			padding: "1%",
			zIndex: 10000,
		}
		return (
			<div>
				<Message error = {this.state.error}> </Message>

				<LeafletMap
					center={this.state.showLocation ? [this.state.userLocation[0], this.state.userLocation[1]]: [this.state.center[0],this.state.center[1]]}
					zoom={this.state.zoom}
					maxZoom={20}
					zoomDelta={0.5}
					zoomControl={false}
				>
					<TileLayer
						url = "https://cdn.digitransit.fi/map/v1/hsl-map/{z}/{x}/{y}.png"
						attribution = "Map data <a href=http://openstreetmap.org>OpenStreetMap </a>, contributors <a href=http://creativecommons.org/licenses/by-sa/2.0/> CC -BY-SA</a>"
					/>
					{ // Do not display the chart if the user is in mobile TODO: Cleanup
						this.state.showChart && !this.isMobileDevice() ? <div onClick = {e => this.toggleDiv(e)} style ={chartAreaStyle}>
							<Chart times = {this.state.stationData}
							/>
							<button style= {buttonStyle} onClick={e => this.updateChart(e,1)}>Mon.</button>
							<button style= {buttonStyle} onClick={e => this.updateChart(e,2)}>Tue.</button>
							<button style= {buttonStyle} onClick={e => this.updateChart(e,3)}>Wed.</button>
							<button style= {buttonStyle} onClick={e => this.updateChart(e,4)}>Thu.</button>
							<button style= {buttonStyle} onClick={e => this.updateChart(e,5)}>Fri.</button>
							<button style= {buttonStyle} onClick={e => this.updateChart(e,6)}>Sat.</button>
							<button style= {buttonStyle} onClick={e => this.updateChart(e,0)}>Sun.</button>
						</div>: null
					}
					<button style = {locationButtonStyle} onClick={e => this.locateUser(e)}>  FIND ME</button>
					{this.state.route ? <Polyline weight= "9" color= "#0000b3" positions = {this.state.route}/>: null}
					{stationMarkers}
					{user}
				</LeafletMap>
			</div>
		);
	}

}

export default Map