import React from "react"
import { Map as LeafletMap, Marker, Popup, TileLayer} from "react-leaflet"
import bikes from "../bikes"
import VehicleMarker from "./VehicleMarker"
import timerMixin from "react-timer-mixin"
import {DivIcon} from "leaflet"
import ReactDOMServer from "react-dom/server"

class Vehicles extends React.Component {

	constructor() {
		super()
		this.state = {
			busses: [],
			bus:{
				lat:0,
				long:0,
				vehicleNumber:0
			},
			lines:[],
			value:"",
			busRoutes:[]
		}
	}

	componentDidMount() {
		bikes.getBusRoutes().then(x => {
			this.setState({busRoutes: x.sort((x,y) => {
				return(x.shortName>y.shortName ? 1 : -1)
				//return(parseInt(x.shortName)-parseInt(y.shortName))
			})})
		})
	}


	componentWillUnmount() { // Clear the intervals so it doesnt leak memory or produce error while in "bike mode"
		clearInterval(this.interval)
	}
	handleSubmit(e) {
		e.preventDefault()
		this.setState(previousState => ({
			lines:[...previousState.lines, previousState.value] // Add the value from the "form" to the buslines
		})
		)
		clearInterval(this.interval) // Clear the old interval because otherwise there will one for every submitted line
		this.interval = timerMixin.setInterval(() => {
			bikes.getMQTT(this.state.value.toUpperCase()).then(response => {

				let filteredBusses = response.filter(bus => {
					return(this.state.lines.includes(bus.route))
				}) // Only include the one's the user has entered in the app (since the server returns all possible data, including other users' lines)
				this.setState({busses: filteredBusses})
			})
		}, 1000);
	}
	handleChange(e) {
		e.preventDefault()
		this.setState({value:e.target.value.toUpperCase()})
	}
	busRouteClick(e){
		e.preventDefault()
		let id = e.target.id.split(":")[1] // The string is in form HSL:2321
		this.setState(previousState => ({
			lines:[...previousState.lines, id]
		})
		)
		clearInterval(this.interval)
		this.interval = timerMixin.setInterval(() => {
			bikes.getMQTT(id).then(response => {

				let filteredBusses = response.filter(bus => {
					return(this.state.lines.includes(bus.route))
				}) // Only include the one's the user has entered in the app (since the server returns all possible data, including other users' lines)
				this.setState({busses: filteredBusses})
			})
		}, 1000)
	}
	secondsToMS(d){
		let endingString = ""
		d < 0 ? endingString = " behind schedule" : endingString = " ahead of schedule"

		if(d==0) endingString = "On schedule"
		d = Math.abs(d) // use absolute value because negative values introduce problems
		let minutes = Math.floor(d%3600/60)
		let seconds = Math.floor(d%3600%60)

		let minutesToDisplay = minutes > 0 ? minutes + (minutes == 1 ? " minute, " : " minutes, ") : "";
		let secondsToDisplay = seconds > 0 ? seconds + (seconds == 1 ? " second" : " seconds") : "";
		return minutesToDisplay + secondsToDisplay + endingString
	}
	render() {
		const markers = this.state.busses.map((bus,i) => {
			let icon = new DivIcon({
				className: "custom-icon",
				html: ReactDOMServer.renderToString(<VehicleMarker busNumber={bus.desi} speed={bus.spd}/>)
			}) // TODO: CLEANUP
			return(bus.lat!=null && bus.long!=null ? // Check for undefined and null
				<Marker key={i+10123+"a"} position={[bus.lat,bus.long]} icon = {icon}>
					<Popup><b>{bus.desi}</b> <br></br>
					Speed: {bus.spd} km/h	<br></br>
						{this.secondsToMS(bus.dl)}</Popup>
				</Marker>:null
			)
		})

		const busRoutes = this.state.busRoutes.map(route => {
			let jaja = route.shortName.length >6 ? 6 : route.shortName.length
			return(
				<li key= {route.gtfsId} id={route.gtfsId} onClick={e => this.busRouteClick(e)}>
					{route.shortName + "-".repeat(6-jaja) + route.longName + " "}  <b>{route.gtfsId.split(":")[1]}</b>
				</li>
			)
		})
		const buttonStyle = {
			position:"fixed",
			backgroundColor: "#b3b3ff",
			top: "10%", left:"1%", border:"2px solid black",
			borderRadius: "50%",
			padding: "1%",
			zIndex: 10000,
		}
		const formStyle = {
			position:"fixed",
			width: "15%",
			top: "5%", left:0, border:"2px solid black",
			zIndex: 10000
		}
		return(
			<div>
				<LeafletMap
					center={[60.17,24.94]}
					zoom={13}
					maxZoom={20}
					zoomDelta={0.5}
					zoomControl={false}
				>
					<TileLayer
						url = "https://cdn.digitransit.fi/map/v1/hsl-map/{z}/{x}/{y}.png"
						attribution = "Map data <a href=http://openstreetmap.org>OpenStreetMap </a>, contributors <a href=http://creativecommons.org/licenses/by-sa/2.0/> CC -BY-SA</a>"
					/>
					{this.state.busses.length >0 ? markers : null}
				</LeafletMap>
				<form onSubmit={e => this.handleSubmit(e)}>
					<label>
						<input style={formStyle} type="text" value={this.state.value} onChange={e => this.handleChange(e)} />
					</label>
					<button style={buttonStyle} onClick = { e => this.handleSubmit(e)}> Track busses</button>
				</form>
				<ul><font size="6"> Click to track</font>
					<font size="3"><i> or enter the bolded number in the form manually</i></font>
					{busRoutes}
				</ul>
			</div>
		)
	}
}
export default Vehicles