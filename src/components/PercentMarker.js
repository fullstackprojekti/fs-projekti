import React from "react";
import PropTypes from "prop-types"

class PercentMarker extends React.Component {
	render() {
		const available = this.props.bikesAvailable || 0
		const max = this.props.spacesAvailable || 0
		const percent = available/(available+max) * 100
		const nearest = this.props.nearest

		const fillColor = available ==0 ? "red" : (available<=3 ? "yellow" : "#33ff5f")
		const nearestColor = "#FF33CC"

		const hole = <circle className="donut-hole" cx="21" cy="21" r="16" fill={nearest ? nearestColor : fillColor} role="presentation"></circle>
		const ring = <circle className="donut-ring" cx="21" cy="21" r="16" fill="transparent" stroke="#d2d3d4" strokeWidth="3" role="presentation"></circle>
		const segment = <circle className="donut-segment" cx="21" cy="21" r="16" fill="transparent" stroke="black" strokeWidth="3" strokeDasharray={`${percent} ${100 - percent}`} strokeDashoffset="25" aria-labelledby="donut-segment-1-title donut-segment-1-desc"></circle>

		return (
			<svg width="50px" height="50px" viewBox="0 0 42 42" className="donut" aria-labelledby="beers-title beers-desc" role="img">
				{hole}
				{ring}
				{segment}
				<g className="chart-text">
					<text className="chart-number" x="35%" y="60%">
						{available}
					</text>
				</g>
			</svg>
		);
	}
}
PercentMarker.propTypes = {
	bikesAvailable: PropTypes.number.isRequired,
	spacesAvailable: PropTypes.number.isRequired,
	nearest: PropTypes.bool
}
export default PercentMarker