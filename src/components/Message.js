import React from "react";
import PropTypes from "prop-types"

class Message extends React.Component {
	render() {
		const errorStyle = {
			border: "2px solid #331400",
			textAlign: "center",
			background: "#ff4d4d",
			fontSize: 22,
			color: "#331400"
		}
		const messageStyle = {
			border: "2px solid #331400",
			textAlign: "center",
			background: "#b3ff66",
			fontSize: 22,
			color: "#331400"
		}
		const warningStyle = {
			border: "2px solid #331400",
			textAlign: "center",
			background: "#ffff66",
			fontSize: 22,
			color: "#331400"
		}


		switch (this.props.error.type) {
		case 0:
			return(
				<div style={messageStyle}>
					{this.props.error.message}
				</div>
			)
		case 1:
			return(
				<div style={warningStyle}>
					{this.props.error.message}
				</div>
			)
		case -1:
			return(
				<div style={errorStyle}>
					{this.props.error.message}
				</div>
			)
		}
	}
}

Message.propTypes = {
	error: PropTypes.any.isRequired,
}

export default Message