import React from "react";
import PropTypes from "prop-types"

class Chart extends React.Component {
	render() {

		const inputData = Object.entries(this.props.times)
		const maxValue = Math.max(...Object.values(this.props.times))
		const width = 100 / inputData.length

		return (
			<div className="chart" style={{height:300}}>
				{
					inputData.map((x,i) => {

						const barStyle = {
							height: (x[1] / maxValue)*100 +"%",
							width: width +"%",
							position:"absolute",
							left: i * width + "%",
							bottom: "10%",
							background: "#38a5e5",
							border: "1px solid white",
							color: "black"
						}
						const xaxisStyle = {
							width: width +"%",
							position:"absolute",
							left: i * width + "%",
							bottom: "10%",
							color: "black",
							zIndex: 50
						}
						let fourDigitTime = x[0].length==3 ? "0"+x[0]:x[0] // Pad with zero
						let time = fourDigitTime.slice(0,2)+":"+fourDigitTime.slice(2)
						return(
							<div key={i+"chart"+2}>
								<div style={barStyle} title={`${x[1]} bikes at ${time}`}> </div>
								{ i%9 == 0 ? <div style={xaxisStyle}> {time}</div>: null}
							</div>
						)
					})
				}
				<div style={{
					position:"absolute",
					left: 0,
					bottom: "20%",
					width:"100%",
					borderTopStyle:"solid",
					borderColor: "#696969",
					borderWidth: "1px",
					color: "black"
				}}> {Math.round(maxValue/5)} </div>

				<div style={{
					position:"absolute",
					left: 0,
					bottom: "40%",
					width:"100%",
					borderTopStyle:"solid",
					borderColor: "#696969",
					borderWidth: "1px",
					color: "black"
				}}> {Math.round(maxValue/5*2)} </div>

				<div style={{
					position:"absolute",
					left: 0,
					bottom: "60%",
					width:"100%",
					borderTopStyle:"solid",
					borderColor: "#696969",
					borderWidth: "1px",
					color: "black"
				}}> {Math.round(maxValue/5*3)} </div>

				<div style={{
					position:"absolute",
					left: 0,
					bottom: "80%",
					width:"100%",
					borderTopStyle:"solid",
					borderColor: "#696969",
					borderWidth: "1px",
					color: "black"
				}}> {Math.round(maxValue/5*4)} </div>

				<div style={{
					position:"absolute",
					left: 0,
					bottom: "100%",
					width:"100%",
					borderTopStyle:"solid",
					borderColor: "#696969",
					borderWidth: "1px",
					color: "black"
				}}> {maxValue} </div>
			</div>
		)
	}
}

Chart.propTypes = {
	times: PropTypes.object
}
export default Chart