import React from "react";
import PropTypes from "prop-types"

class VehicleMarker extends React.Component {
	render() {
		const busNumber = this.props.busNumber || 0
		const speed = this.props.speed || 0

		const fillColor = speed ==0 ? "red" : (speed<=30 ? "yellow" : "#33ff5f")

		const hole = <circle className="donut-hole" cx="21" cy="21" r="16" fill={fillColor} role="presentation"></circle>
		const ring = <circle className="donut-ring" cx="21" cy="21" r="16" fill="transparent" stroke="black" strokeWidth="3" role="presentation"></circle>
		const segment = <circle className="donut-segment" cx="21" cy="21" r="16" fill="transparent" stroke="black" strokeWidth="3" strokeDasharray={"100"} strokeDashoffset="100" aria-labelledby="donut-segment-1-title donut-segment-1-desc"></circle>

		return (
			<svg width="25px" height="25px" viewBox="0 0 42 42" className="donut" aria-labelledby="beers-title beers-desc" role="img">
				{hole}
				{ring}
				{segment}
				<g className="chart-text">
					<text className="chart-number" x="35%" y="60%">
						{busNumber}
					</text>
				</g>
			</svg>
		);
	}
}
VehicleMarker.propTypes = {
	busNumber: PropTypes.string.isRequired,
	speed: PropTypes.number.isRequired
}
export default VehicleMarker