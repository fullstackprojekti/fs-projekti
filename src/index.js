import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App"


ReactDOM.render(
	<App></App>,
	document.getElementById("app")
);
// use if reloading is not desired, e.g., when working with states
//module.hot.accept()