import axios from "axios"

const baseUrl = "https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql"

const getAll = async () => {
	let dataDisplayed = {
		query: `
		{
			bikeRentalStations {
			  name
			  bikesAvailable
			  spacesAvailable
			  lat
			  lon
			  stationId
			  id
			}
		}
	`
	}
	const request = await axios.post(baseUrl,dataDisplayed)
	return request.data
}

const getStation = async (id) => {
	let dataDisplayed = {
		query: `
		{
			bikeRentalStation(id:${JSON.stringify(id)}) {
				name
				bikesAvailable
				spacesAvailable
				lat
				lon
				stationId
				id
			  }
		}
	`
	}
	const request = await axios.post(baseUrl,dataDisplayed)
	return request.data
}
/**Gets the station data from mongoDB
 * @param id the stationId of the station
 * @param day day in number format 0 for sunday, 1 for monday etc.
 */
const getStationDB = async(id,day) => {
	const request = await axios.get(`/api/${id}`, {
		params: {
			day
		}
	})

	return request.data
}
const getNearestStation = async (lat,lon) => {
	let dataDisplayed = {
		query: `
		{
			nearest(lat: ${JSON.stringify(lat)}, lon: ${JSON.stringify(lon)},filterByPlaceTypes:BICYCLE_RENT,maxResults:3) {
				edges {
				  node {
					id,
					distance,
					place{
					  id
					}
				  }
				}
			  }
		}
	`
	}
	const request = await axios.post(baseUrl,dataDisplayed)
	return request.data.data.nearest.edges
}

const getDistance = async (from,to) => {
	let dataDisplayed = {
		query: `
		{
			plan(transportModes:{mode:WALK}, from: {lat: ${JSON.stringify(from[0])}, lon: ${JSON.stringify(from[1])}}, to: {lat: ${JSON.stringify(to[0])}, lon: ${JSON.stringify(to[1])}}) {
			  date
			  itineraries {
				startTime
				endTime
				duration
				waitingTime
				walkTime
				walkDistance
				elevationGained
				elevationLost
			  }
			}
		  }
	`
	}
	const request = await axios.post(baseUrl, dataDisplayed)
	return request.data.data.plan.itineraries[0]
}
/**
 * @returns distance and geometry (coordinates in polyline format) for the route
 */
const getRoute = async(from, to) => {
	let dataDisplayed = {
		query: `
		{
			plan(transportModes:{mode:BICYCLE},
				from: {lat: ${JSON.stringify(from[0])}, lon: ${JSON.stringify(from[1])}},
				 to: {lat: ${JSON.stringify(to[0])}, lon: ${JSON.stringify(to[1])}},
				  numItineraries: 1) {
			  date
			  itineraries {
				legs{
					legGeometry {
						length
						points
					}
					distance
				}
			  }
			}
		  }
	`
	}
	const request = await axios.post(baseUrl, dataDisplayed)
	return request.data.data.plan.itineraries[0]
}

const getBusRoutes = async() => {
	let dataDisplayed = {
		query:`
		{
			routes {
			  gtfsId
			  longName
			  shortName
			}
		  }
	`
	}
	const request = await axios.post(baseUrl, dataDisplayed)
	return request.data.data.routes
}

const getMQTT = async(id) => {
	const http = axios.create({
		baseURL:"/",
		headers: {"Cache-Control":"no-cache"} // Disable caching for some reason?
	})
	const request = await http.get(`/mqtt/${id}`, {
	})

	return request.data

}
export default {getAll, getStation, getNearestStation, getDistance, getStationDB, getRoute,getMQTT, getBusRoutes}