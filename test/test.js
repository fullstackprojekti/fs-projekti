/* eslint-disable no-undef */
let expect = require("chai").expect
let rewire = require("rewire")
let server = rewire("../server")
let bikes = require("../src/bikes")
let polyline = require("polyline")

//let map = rewire("../src/components/Map")
//let MockBrowser = require("mock-browser").mocks.MockBrowser

z
describe("calculate averages for data", () => {
	let calculate = server.__get__("calculateAverageForTimes")

	let inputData = {"0015":
	[ { name: "Töölönlahden puisto",
		bikesAvailable: 0,
		stationId: "150",
		day: 1,
		time: "0015" },
	{ name: "Töölönlahden puisto",
		bikesAvailable: 6,
		stationId: "150",
		day: 1,
		time: "0015" },
	{ name: "Töölönlahden puisto",
		bikesAvailable: 3,
		stationId: "150",
		day: 1,
		time: "0015" },
	{ name: "Töölönlahden puisto",
		bikesAvailable: 1,
		stationId: "150",
		day: 1,
		time: "0015" } ],
	"0030":
	[ { name: "Töölönlahden puisto",
		bikesAvailable: 0,
		stationId: "150",
		day: 1,
		time: "0030" },
	{ name: "Töölönlahden puisto",
		bikesAvailable: 7,
		stationId: "150",
		day: 1,
		time: "0030" },
	{ name: "Töölönlahden puisto",
		bikesAvailable: 3,
		stationId: "150",
		day: 1,
		time: "0030" },
	{ name: "Töölönlahden puisto",
		bikesAvailable: 1,
		stationId: "150",
		day: 1,
		time: "0030" } ],
	"0045":
	[ { name: "Töölönlahden puisto",
		bikesAvailable: 0,
		stationId: "150",
		day: 1,
		time: "0045" },
	{ name: "Töölönlahden puisto",
		bikesAvailable: 6,
		stationId: "150",
		day: 1,
		time: "0045" },
	{ name: "Töölönlahden puisto",
		bikesAvailable: 3,
		stationId: "150",
		day: 1,
		time: "0045" },
	{ name: "Töölönlahden puisto",
		bikesAvailable: 1,
		stationId: "150",
		day: 1,
		time: "0045" } ]}

	it("Output an object literal with the time as a key and average as a value", () => {
		let averages = calculate(inputData)
		// to.eql because we are comparing objects
		expect(averages).to.eql({"0015":3,"0030":3,"0045":3})
	})
})

/*describe("map", () => {
	let checkDevice = map.__get__("isMobileDevice")
	let mock = new MockBrowser()
	let doc = mock.getDocument()
	let div = doc.createElement("div")
	console.log(div)
	it("shouldnt be", () => {
		let jaja = checkDevice()
		expect(jaja).to.be.equal(false)
	})
})*/

describe("testing the routes api",  () => {
	// Not sure why I have to write the default
	it("should return a distance that is a number",async() => {
		bikes.default.getRoute([60.17,24.94], [60.22,24.95]).then(
			(x) => {
				//console.log(legs[0].legGeometry)
				expect(x.legs[0].distance).to.be.a("number")
			//	process.exit()
			}, (error) => {
				console.log(error)
			})
		/*bikes.getAll().then(x => console.log(x))
		let jaja = bikes.getRoute([60.17,24.94], [60.22,24.95])*/
		//console.log(Promise.resolve(getdist))
		//console.log(getdist.should.eventually.equal(4))
	//	return getdist.should.be.fulfilled
	})

	it("should return coordinates",async() => {
		bikes.default.getRoute([60.17,24.94], [60.22,24.95]).then(
			(x) => {
				//console.log(legs[0].legGeometry)

				let coordinates = polyline.decode(x.legs[0].legGeometry.points)
				expect(coordinates).to.be.a("array")
			}, (error) => {
				console.log(error)
			})
	})

	it("the latitude should be a number",async() => {
		bikes.default.getRoute([60.17,24.94], [60.22,24.95]).then(
			(x) => {
				//console.log(legs[0].legGeometry)
				let coordinates = polyline.decode(x.legs[0].legGeometry.points)
				expect(coordinates[0][0]).to.be.a("number")
				process.exit() // Hacky way to exit the process because the async function has some issues
			}, (error) => {
				console.log(error)
			})
	})

})